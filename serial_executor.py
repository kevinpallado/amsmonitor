import serial
import re
import sqlite3 as lite
import requests
import os.path
import json
import datetime

file_path = os.path.dirname(os.path.realpath(__file__)) + '/setting.ini'
print(file_path)
setting_data = {}
student = []
oldTag = ''
newTag = ''

port = serial.Serial(
        "/dev/ttyUSB0",
        baudrate=9600,
        parity=serial.PARITY_NONE,
        bytesize=serial.EIGHTBITS,
        stopbits=serial.STOPBITS_ONE,
        timeout=0.1)

def scan_id():
    global student, imageurl, oldTag, newTag
    if os.path.exists(file_path):
        with open(file_path, "r") as ins:
            for line in ins:
                if line.find('=') != -1:
                    split_data = line.strip('\n').split('=')
                    if len(split_data) == 2:
                        setting_data[split_data[0]] = split_data[1]

    imageurl = setting_data["ImageUrl"]
    while True:
        #if setting_data.has_key("IP") and setting_data.has_key("Server"):
        if "IP" in setting_data and "Server" in setting_data:
            rcv = port.readline()
            # bytes to string conversion
            str_by = str(rcv,encoding='utf-8')
            rcvdata = re.sub('[^A-Za-z0-9]+', '', str_by)
            newTag = rcvdata if rcvdata != '' else newTag
            #print("newtag = %s, rcvdata =  %s, oldtag = %s" %(newTag,rcvdata,oldTag))

            if newTag != oldTag:
                #print("Same Tag")
            #else:
                oldTag = newTag
                #print("Not same tag")
                if (rcvdata != ''):
                    try:
                        print(rcvdata);
                        #url = 'http://' + setting_data["Server"]+'/k12/public/tap/ams/'+ setting_data["tap"] +'/'+rcvdata
                        url = 'http://' + setting_data["Server"] + '/k12_delmar/public/tap/ams?status=' + setting_data["tap"] +'&tag='+rcvdata+'&machine=' + setting_data["Machine"]
                        #print("SEND REQUEST !!!!!");
                        response = requests.get(url,timeout=1)
                        data = response.text
                        #print(data)
                        result = json.loads(data)
                        if (result['status']):
                            student = [result['IdNumber'],result['FirstName'],result['MiddleInitial'],result['LastName'],result['Position'],result['status']]
                    except (requests.exceptions.Timeout,ValueError,requests.exceptions.ConnectionError) as e:
                        with open("errorLogs.txt","a") as errorText:
                            errorText.write("{} => {}\n".format(format(datetime.datetime.now(), '%b %d, %Y - %I:%M:%S %p'),e))
                            print("Error logs: {}".format(e))
                            errorText.close()
                        #print(str(e))
                        #errorText = open("errorLogs.txt","w")
                        #errorText.write(e)
                        #errorText.close()
                        pass

                    #time.sleep(delay)
                    port.flushInput()
                    port.flushOutput()
