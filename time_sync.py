import requests
import json
import os

#file_path = os.path.dirname(os.path.realpath(__file__)) + '/setting.ini'
file_path = '/home/pi/amsmonitor/setting.ini'
print(file_path)
setting_data = {}

if os.path.exists(file_path):
    with open(file_path, "r") as ins:
        for line in ins:
            if line.find('=') != -1:
                split_data = line.strip('\n').split('=')
                if len(split_data) == 2:
                    setting_data[split_data[0]] = split_data[1]

if "IP" in setting_data and "Server" in setting_data:
    try:
        #url = 'http://' + setting_data["Server"] +'/API?type=sync_time'
        url = 'http://' + setting_data["Server"] + '/k12_delmar/public/API?type=sync_time'
        response = requests.get(url,verify=False,timeout=20)
        print (response)
        data = response.text
        result = json.loads(data)
        cmd = "sudo date -s '%s %s'"%(result['date'],result['time'])
        print(cmd)
        os.system(cmd)
        #cmd = ['sudo date +%F -s '+result['date'], 'sudo date +%X -s '+result['time']]
            #for command in range(len(cmd)):
            #    print(cmd[command])
           #    os.system(cmd[command])
    except (requests.exceptions.Timeout,ValueError,requests.exceptions.ConnectionError) as e:
        f = open('/home/pi/log.txt','w')
        f.write('%s\n' %e)
        f.close()
