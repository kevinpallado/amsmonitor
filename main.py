#!usr/bin/python
import os
os.environ['KIVY_GL_BACKEND'] = 'gl'
import threading
import kivy
import time
import serial_executor
import datetime
from kivy.app import App
from kivy.uix.image import Image , AsyncImage
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.config import Config
from kivy.properties import ObjectProperty
from kivy.properties import StringProperty

Config.set("graphics", "resizable", False)
Config.set("graphics", "width", "1600")
Config.set("graphics", "height", "900")
Config.set('graphics', 'fullscreen', 1)
Builder.load_file("kv_layout/information.kv")

serial_class = serial_executor.scan_id
t1 = threading.Thread(target=serial_class, args=())
student_present = []
present_student = " "
startTimer = " "
ScreensaverTime = " "

def ScreenSaverTime(student):
    time_ticking = datetime.datetime.now()
    global present_student,startTimer, ScreensaverTime
    if present_student == student:
        if startTimer:
           ScreensaverTime = datetime.datetime.now() + datetime.timedelta(seconds=15)
           startTimer = False
           resetTags = False
        else:
           if format(ScreensaverTime, '%H:%M:%S') == format(time_ticking, '%H:%M:%S'):
               print("SCREEN SAVER")
               serial_executor.student = []
               startTimer = " "
               serial_executor.newTag = " "
               serial_executor.oldTag = " "
    else:
        present_student = student
        startTimer = True
        screenSaverBool = False

class Information(FloatLayout):
    global screenSaverBool

    in_user = ObjectProperty(None)
    out_user = ObjectProperty(None)
    user_name = ObjectProperty(None)
    lastname = ObjectProperty(None)
    u_time = ObjectProperty(None)
    user_section = ObjectProperty(None)
    image_src = StringProperty(None)
    image_bg = StringProperty(None)
    name_label = ObjectProperty(None)
    section_label = ObjectProperty(None)

    def update(self, *args):
        self.time = time.asctime()
        student = serial_executor.student
        imageurl = serial_executor.imageurl
        if(len(student) > 0):
            ScreenSaverTime(student[1])
            self.name_label.text = "Name : "
            self.user_name.text = student[1] if not student[2] else student[1] + " " + student[2]
            self.user_section.text = student[4]
            self.lastname.text = student[3]
            self.u_time.text = format(datetime.datetime.now(), '%b %d, %Y - %I:%M:%S %p')
            self.image_src = str(imageurl) + str(student[0]) + ".jpg"
            self.image_bg = "pictures/delmar.jpg"

            if student[5]=='0':
                self.out_user.text = "OUT"
                self.in_user.text = " "
            else:
                self.in_user.text = "IN"
                self.out_user.text = " "
        else:
            self.user_name.text = " "
            self.lastname.text = " "
            self.image_src = "pictures/transparent.png"
            self.name_label.text = " "
            self.section_label.text = " "
            self.u_time.text = " "
            self.user_section.text = " "
            self.in_user.text = " "
            self.out_user.text = " "
            self.image_bg = "pictures/delmarscreensaver.jpg"

class Confirmation(FloatLayout):
    pass

class PrincetechApp(App):
    def build(self):
        self.load_kv("kv_layout/update.kv")
        c = Confirmation()
        stat = c.ids.stat
        Clock.schedule_interval(stat.update, 0)
        return c


if __name__ == "__main__":
    t1.start()
    PrincetechApp().run()
